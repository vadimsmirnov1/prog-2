// Application.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include"..\Library\Circle.h"




int getNum(double &a){ 
int n; 
do{ 
	cin>>a;
	if(!std::cin.good()){
		n=0;
		std::cout<<"Wrong input, repeat"<<std::endl;
		cin.clear();
		cin.sync();
	}
	else 
		n=1;
} while (n == 0); 
return 1;
}

int getNum(float &a){ 
int n; 
do{ 
	cin>>a;
	if(!std::cin.good()){
		n=0;
		std::cout<<"Wrong input, repeat"<<std::endl;
		cin.clear();
		cin.sync();
	}
	else 
		n=1;
} while (n == 0); 
return 1;
}


int getNum(int &a)
{ 
int n; 
do{ 
	cin>>a;
	if(!std::cin.good()){
		n=0;
		std::cout<<"Wrong input, repeat"<<std::endl;
		cin.clear();
		cin.sync();
	}
	else 
		n=1;
} while (n == 0); 
return 1;
}

void menu(){
cout<<" 1 - Enter 1 point or there are NO points \n 2 - Enter many points \n 3 - Output \n 4 - Center of gravity \n 5 - Show point \n 6 - Do turn \n 7 - Move polygon \n 8 - Initialization \n 9 - Overloaded output \n 10 - Overloaded Input \n 11 - Overloaded + \n 12 - Overloaded [] \n" ;

}

int _tmain(int argc, _TCHAR* argv[])
{
	polygon t;
	twovalue H;
	int Y,T; bool k;
	int count =1,x1,y1,j;
	cout.setf(ios_base::fixed); 
	cout.precision(1);
	menu();
	while(count!=0){
		std::cout<<"Enter your choice"<<std::endl;;
		getNum(count);
		switch(count){

			case 1:{
				try{
				t.checkN();

				cout<<"Enter X and Y of "<<t.getN()+1<<" point" <<endl;
				getNum(x1);
				getNum(y1);
				t.create1point(x1,y1);
				
				}
				catch(invalid_argument& e){
					cout << e.what() << endl;
					break;
				};

				break;
			}
			case 2:{
				cout<<"Enter the number of points you want to enter"<<endl;
				getNum(j);
				if (t.getN()+j>t.returnU()){
					cout<<"Too much numbers1!!"<<endl;
					break;
				}
				for(int i = 0;i<j;i++){
					cout<<"Enter X and Y of "<<t.getN()+1<<" point" <<endl;
					getNum(x1);
					getNum(y1);
					t.create1point(x1,y1);
				}
		
				break;	   
			}

			case 3:{
					t.output();
				break;	   
			}

			case 4:{
				try{
					H = t.centerofgravity();
				}
				catch(invalid_argument& e){
					cout << e.what() << endl;
					break;
				}

				cout<<"Center - ("<<H.x<<","<<H.y<<")"<<endl;

				break;
			}
			
			case 5:{
				cout<<"Enter number of point"<<endl;
				getNum(x1);
				try{
					H = t.returnpoint(x1);
				}
				catch(invalid_argument& e){
					cout << e.what() << endl;
					break;
				}
				cout<<"("<<H.x<<","<<H.y<<")"<<endl;
				break;
			}
			case 6:{
				double grad;
				cout<<"Enter grad"<<endl;
			    getNum(grad);
				cout<<"Enter number of point"<<endl;
				getNum(x1);
				t.doturn(x1,grad);
				break;	   
			}
			case 7:{
				cout<<"Enter X and Y of radius vector"<<endl;
				getNum(x1);
				getNum(y1);
				t.movevector(x1,y1);
				break;
				   }
			case 8:{
				   cout<<"Enter 1 to enter the number of initialized mass or 2 to enter initialized point"<<endl;
					twovalue* mass1;
					getNum(Y);
					if(Y == 1){
						cout<<"Enter the number of points"<<endl;
						getNum(T);
						T = abs(T);
					
							
						
						try{
							//mass1 = new twovalue [T];
							mass1 = (twovalue *)malloc(T * sizeof(twovalue ));
						}
						catch(std::bad_alloc &ba){
							cout<< ba.what()<<endl;
							cout<<"Houston, we've had a problem"<<endl;
							break;
						}

						for(int i = 0;i<T;i++){
							k = false;
							cout<<"Enter X Y of "<<i+1<<" point"<<endl;
							do {
								if(k)
									cout<<"Already in mass, ReEnter1!"<<endl;
								getNum(x1);
								getNum(y1);
								k = false;
								for(int j = 0;j<i;j++)
									if ( (mass1[j].x == x1)&&(mass1[j].y == y1) )
										k = true;
								
								
							}while(k);
							mass1[i].x = x1;
							mass1[i].y = y1;
						}
						try{
							 polygon X(T,mass1);
							// delete [] mass1;
							 t = X;
						}
						catch(invalid_argument& e){
							delete [] mass1;
							cout << e.what() << endl;
							break;
						}
			
						
					}
					
					if (Y==2){
							cout<<"Enter X Y"<<endl;
							getNum(x1);
							getNum(y1);
							polygon X(x1,y1);
							t = X;
							
					}
				   break;
			}

			case 9 :{
					
					cout<<t;
					
					break;}
			case 10:{

					try{
						t.checkN();
					}
					catch(invalid_argument& e){
						cout << e.what() << endl;
						break;
					};

					
					cout<<"Enter X Y"<<endl;
					cin>>t;
					while(!cin){
						cout<<"Wrong input, repeat"<<endl;
						cin.clear(std::ios::goodbit);
						cin.sync();
						cin>>t;
					}
					t.checkcin();
					break;}

			case 11:{
					cout<<"Enter X Y"<<endl;
					if(t.getN() == t.returnU()){
						cout<<"OverSize!1"<<endl;
						break;
					}
					
					getNum(x1);
					getNum(y1);
					if(!t.checkmass(x1,y1)){
						cout<<"Such point is already in mass"<<endl;
						break;
					}

					
					polygon X(x1,y1);
					//t.myrealloc(1);
					t += X;
					
					
					break;
			}


			case 12:{
				cout<<"Enter the number of point"<<endl;
				getNum(Y);
				Y=abs(Y);
				if(Y>t.getN()||(Y == 0)){
					cout<<"There is NO such point!11"<<endl;
					break;

				}

				cout<<"("<<t[Y].x<<","<<t[Y].y<<")"<<endl;
					
				break;}
			case 0:{
			   break;
			}
			default:
				std::cout<<"Choice is incorrect"<<std::endl;;
		
		}
	}

	system("pause");
	return 0;
}

