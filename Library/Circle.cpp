#include "StdAfx.h"
#include "Circle.h"

#include <cstddef>



void polygon::create1point(float x1,float y1){
		int i;
		for (i=0;i<N;i++)
			if( (mass[i].x == x1)&&(mass[i].y == y1) )
				throw invalid_argument ("Such element is already in mass!!1");

		if(myrealloc(1)){

			N+=1;
			setmass(x1,y1);
		}
}



twovalue polygon::centerofgravity(){
	twovalue H;
	if(N==0)
		throw invalid_argument("There are NO points!1!");
	if(N==1)
		throw invalid_argument("There only 1 point1!!");
	if(N==2)
		throw invalid_argument("There only 2 points!!1");
	float sum = 0;
	for(int i =0;i<N;i++)
		sum+= mass[i].x;

	H.x =(sum)/N;
	sum = 0;
	for(int i =0;i<N;i++)
		sum+= mass[i].y;
	H.y = (sum)/N;
	return H;
}

void polygon::output(){
	cout.setf(ios_base::fixed); 
	cout.precision(1);
	cout<<"N = "<<N<<endl;
	for(int i = 0;i<N;i++){
		cout<<i+1<<" ("<<mass[i].x<<","<<mass[i].y<<")"<<endl;
	}
}

twovalue polygon::returnpoint(int i){
	if (i>N)
		throw invalid_argument("No point with such number!1!");
	return mass[i-1];
}


void polygon::doturn(int j,float grad){
	int i,x1,y1,x2,y2;
	grad = PI*grad/180;
	x2 = mass[j-1].x;
	y2 = mass[j-1].y;
	for(i=0;i<N;i++){
		x1 = mass[i].x;
		y1 = mass[i].y;
		mass[i].x = x2 + (x1 - x2)*cos(grad) - (y1 - y2)*sin(grad);
		mass[i].y = y2 + (x1 - x2)*sin(grad) + (y1 - y2)*cos(grad);
	}		
}

void polygon::movevector(float x1, float y1){
	for(int i =0;i<N;i++){
		mass[i].x+=x1;
		mass[i].y+=y1;
	}
}

bool polygon::checkmass(float x1, float y1){
	bool k = true;
	for(int i =0;i<N;i++)
		if( (mass[i].x == x1)&&(mass[i].y == y1) )
			k = false;
	return k;

}

void polygon::checkcin(){
	N-=1;
	if(!checkmass(mass[N].x,mass[N].y))
		return;
	N+=1;



}

bool polygon::myrealloc(int t){
	t = abs(t);
	twovalue* mass1;
	size_t k = sizeof(*mass);
	try{
		mass1 = new twovalue [N+t];
		}
	catch(std::bad_alloc &ba){
		cout<< ba.what()<<endl;
		cout<<"Houston, we've had a problem"<<endl;
		return false;
	}
	for(int i = 0;i<N;i++){
		mass1[i].x = mass[i].x;
		mass1[i].y = mass[i].y;
	}
	if(N!=0)
		delete [] mass;
	mass = mass1;
	return true;
}